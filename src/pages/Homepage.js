import React from 'react'
import Header from '../components/header/header'
import MainPartOne from '../components/main/mainPartOne/mainPartOne'
import MainPartTwo from '../components/mainPartTwo/mainPartTwo'
import MainPartThree from '../components/mainPartThree/mainPartThree'
import ListProduct from '../components/listProduct/listProduct'
import Footer from '../components/footer/footer'
import home from './homepage.module.css'

const Homepages = () => {
	return (
		<div>
			<div className={home.homepage}>
				<Header></Header>
			</div>
			<MainPartOne></MainPartOne>
			<MainPartTwo></MainPartTwo>
			<MainPartThree></MainPartThree>
			<ListProduct></ListProduct>
			<Footer></Footer>
		</div>
	)
}

export default Homepages
