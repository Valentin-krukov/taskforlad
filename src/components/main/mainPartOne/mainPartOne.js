import React from 'react'
import m from './mainPartOne.module.css'

const MainPartOne = () => {
	return (
		<div className={m.mainPartOne}>
			<div className={m.mainPartOne__tagline}>
				<p className={m.mainPartOne__title}>
					We Help You Make Modern Furniture
				</p>
				<p className={m.mainPartOne__subtitle}>
					We will help you to make an elegant and luxurious interior designed by
					professional interior designer.
				</p>
			</div>
			<img className={m.mainPartOne__pic} src='/rectangle.jpg' alt='Room' />
		</div>
	)
}
export default MainPartOne
