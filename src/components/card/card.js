import React from 'react'
import { useSelector } from 'react-redux'
import c from './card.module.css'
import { useDispatch } from 'react-redux'
import { addProduct } from '../../store/product/product.slice'
import BtnCard from './btnCard'

const Card = ({ id, img, nameProduct, price }) => {
	const dispatch = useDispatch()

	const basket = useSelector(state => state.product.basket)

	let item = {
		id: id,
		img: img,
		nameProduct: nameProduct,
		price: price,
	}

	const handleAddProduct = () => {
		dispatch(
			addProduct({
				...item,
			})
		)
	}

	return (
		<div className={c.card}>
			<div className={c.card__content}>
				<img className={c.card__pic} src={img} alt='product' />
				<div className={c.card__stuff}>
					<p className={c.card__nameProduct}>{nameProduct} </p>
					<div className={c.card__stuff__icon}>
						<BtnCard></BtnCard>
						<img src='./icon.svg' alt='icon' />
					</div>
				</div>
				<div className={c.card__price}>
					<p className={c.card__price__dollars}>${price}</p>
					<button
						disabled={basket.find(item => item.id === id) ? true : false}
						onClick={handleAddProduct}
						className={c.card__btn}
					>
						Buy now
					</button>
				</div>
			</div>
		</div>
	)
}
export default Card
