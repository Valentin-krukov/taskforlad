import React, { useState } from 'react'
import c from './card.module.css'

const BtnCard = ({ id }) => {
	const [like, setLike] = useState()
	const likeProduct = () => {
		if (!like) {
			setLike(true)
		} else {
			setLike(false)
		}
		localStorage.setItem(id, like)
	}

	return (
		<div>
			{like ? (
				<button onClick={likeProduct} className={c.card__stuff__icon__like}>
					<img src='./like.svg' alt='like' />
				</button>
			) : (
				<button onClick={likeProduct} className={c.card__stuff__icon__like}>
					<img src='./likePassive.svg' alt='like' />
				</button>
			)}
		</div>
	)
}
export default BtnCard
