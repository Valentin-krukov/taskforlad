import React from 'react'
import l from './listProduct.module.css'
import Card from '../card/card'
import { useGetMerchandiseQuery } from '../../store/api/api'

const ListProduct = () => {
	const { isLoading, data } = useGetMerchandiseQuery()

	return (
		<div id='furniture' className={l.listProduct}>
			<div className={l.listProduct__adaptive}>
				<h2 name='homeElement' className={l.listProduct__title}>
					Our Popular Furniture
				</h2>
				<p className={l.listProduct__subtitle}>
					All of our furniture uses the best materials and choices for our
					customers.All of our furniture uses the best materials and choices for
					our customers.
				</p>
			</div>

			<div className={l.listProduct__card}>
				{isLoading ? (
					<div>Loading...</div>
				) : data ? (
					data.map(merchandise => (
						<Card
							key={merchandise.id}
							merchandise={merchandise}
							id={merchandise.id}
							img={merchandise.img}
							nameProduct={merchandise.nameProduct}
							price={merchandise.price}
						/>
					))
				) : (
					<div>Not found</div>
				)}
			</div>
		</div>
	)
}
export default ListProduct
