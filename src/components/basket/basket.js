import React from 'react'
import b from './basket.module.css'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { removeProductBasket } from '../../store/product/product.slice'

const Basket = () => {
	const basket = useSelector(state => state.product.basket)
	const dispatch = useDispatch()
	const removeProduct = id => {
		dispatch(removeProductBasket(id))
	}
	const priceProducts = useSelector(state => state.product.priceProducts)
	return (
		<div className={b.basket}>
			<div className={b.basket__content}>
				<h2 className={b.basket__title}>КОРЗИНА С ВЫБРАННЫМИ ТОВАРАМИ</h2>
				<p className={b.basket__count}>
					<span className={b.basket__dollar}>$ </span>
					{priceProducts}
				</p>
				<Link to='/'>
					<button className={b.basket__btn}>Назад</button>
				</Link>
			</div>
			<div className={b.basket__item}>
				{basket.map((item, index) => {
					const { img, nameProduct, price, id } = item
					return (
						<div key={item.id} className={b.card}>
							<div className={b.card__content}>
								<img className={b.card__pic} src={img} alt='product' />
								<div className={b.card__stuff}>
									<p className={b.card__nameProduct}>{nameProduct} </p>
									<div className={b.card__stuff__icon}>
										<img src='./icon.svg' alt='icon' />
									</div>
								</div>
								<div className={b.card__price}>
									<p className={b.card__price__dollars}>${price}</p>
									<button
										onClick={() => removeProduct(id)}
										className={b.card__btn}
									>
										Delete
									</button>
								</div>
							</div>
						</div>
					)
				})}
			</div>
		</div>
	)
}

export default Basket
