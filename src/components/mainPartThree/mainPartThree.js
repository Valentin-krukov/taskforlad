import React from 'react'
import three from './mainPartThree.module.css'
const MainPartThree = () => {
	return (
		<div id='about' className={three.mainPartThree}>
			<div className={three.mainPartThree__about}>
				<h2 className={three.mainPartThree__title}>About Us</h2>
				<p className={three.mainPartThree__subtitle}>
					All of our furniture uses the best materials and choices for our
					customers.All of our furniture uses the best materials
				</p>
				<div className={three.mainPartThree__advantage}>
					<div className={three.mainPartThree__advantage__items}>
						<img
							className={three.mainPartThree__advantage__items__logo}
							src='./best.png'
							alt='Best Quality'
						/>
						<div>
							<h3 className={three.mainPartThree__advantage__items__title}>
								Best Quality
							</h3>
							<p className={three.mainPartThree__advantage__items__subtitle}>
								All of our furniture uses the best materials and choices
							</p>
						</div>
					</div>
					<div className={three.mainPartThree__advantage__items}>
						<img
							className={three.mainPartThree__advantage__items__logo}
							src='./shopping.png'
							alt='Free Shopping'
						/>
						<div>
							<h3 className={three.mainPartThree__advantage__items__title}>
								Free Shopping
							</h3>
							<p className={three.mainPartThree__advantage__items__subtitle}>
								All of our furniture uses the best materials and choices
							</p>
						</div>
					</div>
					<div className={three.mainPartThree__advantage__items}>
						<img
							className={three.mainPartThree__advantage__items__logo}
							src='./transport.png'
							alt='Free Shopping'
						/>
						<div>
							<h3 className={three.mainPartThree__advantage__items__title}>
								Free Shopping
							</h3>
							<p className={three.mainPartThree__advantage__items__subtitle}>
								All of our furniture uses the best materials and choices
							</p>
						</div>
					</div>
				</div>
			</div>
			<img className={three.mainPartThree__room} src='./room.jpg' alt='Room' />
		</div>
	)
}
export default MainPartThree
