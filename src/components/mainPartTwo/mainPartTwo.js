import React from 'react'
import two from './mainPartTwo.module.css'

const MainPartTwo = () => {
	return (
		<div className={two.mainPartTwo}>
			<h2 className={two.mainPartTwo__title}>Trusted by 20,000+ companies</h2>
			<div className={two.mainPartTwo__logo}>
				<img src='./mastercard.png' alt='Mastercard' />
				<img
					className={two.mainPartTwo__logo__airbnb}
					src='./airbnb.png'
					alt='Airbnb'
				/>
				<img src='./uber.png' alt='Uber' />
				<img src='./paypal.png' alt='PayPal' />
				<img src='./visa.png' alt='Visa' />
				<img src='./stripe.png' alt='Stripe' />
			</div>
		</div>
	)
}
export default MainPartTwo
