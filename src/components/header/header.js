import React, { useState } from 'react'
import h from './header.module.css'
import BasketBtn from './btnBasket'
import { Link } from 'react-scroll'

const Header = () => {
	const [active, passive] = useState(false)
	const classActive = [[h.header], [h.open]].join(' ')
	const classPassive = [h.header]
	const ActiveBtn = () => {
		if (!active) {
			passive(true)
		} else {
			passive(false)
		}
	}
	return (
		<header id='home' className={active ? classActive : classPassive}>
			<button onClick={ActiveBtn} className={h.header_burger}>
				<span></span>
				<span></span>
				<span></span>
			</button>
			<Link to='home' spy={true} smooth={true} offset={-5000} duration={2000}>
				<h1 className={h.header__title}>DudeShape</h1>
			</Link>

			<nav className={h.header__menu__nav}>
				<ul className={h.header__nav}>
					<Link
						to='home'
						spy={true}
						smooth={true}
						offset={-5000}
						duration={2000}
					>
						<li className={h.header__title_hover}>Home</li>
					</Link>
					<Link
						to='about'
						spy={true}
						smooth={true}
						offset={-100}
						duration={2000}
					>
						<li className={h.header__title_hover}>About</li>
					</Link>
					<Link
						to='furniture'
						spy={true}
						smooth={true}
						offset={-100}
						duration={2000}
					>
						<li className={h.header__title_hover}>Furniture</li>
					</Link>
					<Link
						to='contact'
						spy={true}
						smooth={true}
						offset={-100}
						duration={2000}
					>
						<li className={h.header__title_hover}>Contact</li>
					</Link>
				</ul>
			</nav>
			<div className={h.header__menu}>
				<img className={h.header__search} src='/search.svg' alt='Поиск'></img>
				<BasketBtn></BasketBtn>
			</div>
		</header>
	)
}
export default Header
