import React from 'react'
import { Link } from 'react-router-dom'
import h from './btnBasket.module.css'
import { useSelector } from 'react-redux'

const BasketBtn = () => {
	const count = useSelector(state => state.product.count)
	return (
		<Link to='/basket'>
			<img className={h.header__basket} src='/cart.svg' alt='Корзина'></img>
			<p className={h.btn}>{count}</p>
		</Link>
	)
}
export default BasketBtn
