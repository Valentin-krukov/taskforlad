import React from 'react'
import f from './footer.module.css'
const Footer = () => {
	return (
		<div id='contact' className={f.footer}>
			<div className={f.footer__container}>
				<div>
					<h3 className={f.footer__title}>DudeShape</h3>
					<p className={f.footer__overview}>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod
						tempor incididunt ut labore at dolore.
					</p>
					<p className={f.footer__title__social}>Follow Us :</p>
					<div className={f.footer__social}>
						<img src='./facebook.svg' alt='Facebook' />
						<img src='./twit.svg' alt='Twitter' />
						<img src='./youtube.svg' alt='YouTube' />
						<img src='./inst.svg' alt='Instagram' />
					</div>
				</div>
				<div>
					<h3 className={f.footer__title}>Take a tour</h3>
					<div className={f.footer__category}>
						<p>Features</p>
						<p>Pricing</p>
						<p>Product</p>
						<p>Support</p>
					</div>
				</div>
				<div>
					<h3 className={f.footer__title}>Our company</h3>
					<div className={f.footer__category}>
						<p>About Us</p>
						<p>Blog</p>
						<p>Media</p>
						<p>Contact Us</p>
					</div>
				</div>
			</div>
		</div>
	)
}
export default Footer
