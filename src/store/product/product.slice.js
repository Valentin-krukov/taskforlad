import { createSlice } from '@reduxjs/toolkit'

const productSlice = createSlice({
	name: 'basket',
	initialState: {
		basket: [],
		count: 0,
		priceProducts: 0,
	},
	reducers: {
		addProduct(state, action) {
			state.basket.push(action.payload)
			state.count = ++state.count
			state.priceProducts = state.basket.reduce((sum, current) => {
				return sum + +current.price
			}, 0)
		},
		removeProductBasket(state, action) {
			state.basket = state.basket.filter(item => item.id !== action.payload)
			state.count = --state.count
			state.priceProducts = state.basket.reduce((sum, current) => {
				return sum + +current.price
			}, 0)
		},
	},
})
export const { addProduct } = productSlice.actions
export const { removeProductBasket } = productSlice.actions

export default productSlice.reducer
