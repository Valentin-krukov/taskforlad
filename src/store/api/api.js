import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

const API_URL = 'http://localhost:3001/product'

export const api = createApi({
	reducerPath: 'api',
	tagTypes: ['Merchandise'],
	baseQuery: fetchBaseQuery({
		baseUrl: API_URL,
	}),
	endpoints: builder => ({
		getMerchandise: builder.query({
			query: () => '/',
		}),
		newMerchandise: builder.mutation({
			query: merchandise => ({
				body: merchandise,
				url: '/',
				method: 'POST',
			}),
		}),
	}),
})
export const { useGetMerchandiseQuery } = api
