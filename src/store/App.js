import React from 'react'
import a from './App.module.css'
import Homepages from './pages/Homepage'
import Basketpage from './pages/Basketpage'
import { Routes, Route } from 'react-router-dom'

const App = () => {
	return (
		<div className={a.container}>
			<Routes>
				<Route path='/' element={<Homepages></Homepages>}></Route>
				<Route path='/basket' element={<Basketpage></Basketpage>}></Route>
			</Routes>
		</div>
	)
}

export default App
